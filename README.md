# SAXS nanoparticles DS
The dataset is divided in 4 sub-datasets : synth_xeuss_1800_HR, synth_nano_HR, real_xeuss_1800_HR and real_nano_HR.
For synthetic DS, each sample contains the normalized intensity vector I (cm-1), the q vector (A-1), nanoparticles structural parameters used for SaSview simulation, and class label in field 'group'.
Real DS are made up of 10 samples each. Each sample is identified by its name (e.g. 'sphere_1') and contains the associated normalized intensity vector and q vector.
